export default function countTypesNumber(source) {
  // TODO 6: 在这里写实现代码
  const result = Object.values(source).map(item => parseInt(item, 10));
  return result.reduce((accumulate, current) => accumulate + current);
}
